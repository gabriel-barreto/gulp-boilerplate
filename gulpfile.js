const gulp = require('gulp');
const sass = require('gulp-sass');
const clean = require('gulp-clean');
const watch = require('gulp-watch');
const uglify = require('gulp-uglify');
const concat = require('gulp-concat');
const autoprefixer = require('gulp-autoprefixer');
const sourcemaps = require('gulp-sourcemaps');
const jshint = require('jshint');
const eventStream = require('event-stream');
const runSequence = require('run-sequence');

'use strict';

//for convert .scss in .css
gulp.task('sass', function() {
    gulp.src('./stylesheets/scss/main.scss')
        .pipe(sass.sync().on('error', sass.logError))
        .pipe(sourcemaps.init())
        .pipe(autoprefixer({
            browsers: ['last 2 version'],
            cascade: false
        }))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./stylesheets/css'));
});

//for debug files .js
gulp.task('jshint', function() {
    return gulp.src('js/**/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

//for concat and uglify my angular scripts
gulp.task('uglifyApp', ['cleanAppJs'], function() {
    return gulp.src('./scripts/js/**/*.js')
        .pipe(concat('appAll.min.js'))
        .pipe(gulp.dest('scripts/'));
});

//for concat and uglify my libraries .js files
gulp.task('uglifyLib', ['cleanLibJs'], function() {
    return gulp.src([
            './node_modules/jquery/dist/jquery.min.js',
            './node_modules/tether/dist/js/tether.min.js',
            './node_modules/bootstrap/dist/js/bootstrap.min.js',
            './node_modules/angular/angular.min.js',
            './node_modules/angular-route/angular-route.min.js'
        ])
        .pipe(concat('libAll.min.js'))
        .pipe(gulp.dest('scripts/'));
});

//for clean my libraries .min.js file
gulp.task('cleanLibJs', function() {
    gulp.src('./scripts/libAll*.*')
        .pipe(clean());
});

//for clean my application .min.js file
gulp.task('cleanAppJs', function() {
    gulp.src('./scripts/appAll*.*')
        .pipe(clean());
});

//for clean my stylesheet .css file
gulp.task('cleanCss', function() {
    gulp.src('./stylesheets/css/main.css')
        .pipe(clean());
});

//for watch modification in my .scss files
gulp.task('sass:watch', function() {
    gulp.watch('./stylesheets/scss/**/*.scss', ['sass']);
});